package com.zuitt.batch193;

public interface Greetings {
    public void morningGreet();
    public void holidayGreet();
}
