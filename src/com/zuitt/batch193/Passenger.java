package com.zuitt.batch193;

public class Passenger {
    // property
    private String passengerName;
    // constructor
    public Passenger(String passengerName){
        this.passengerName = passengerName;
    }
    // getter
    public String getPassengerName(){
        return this.passengerName;
    }
}
