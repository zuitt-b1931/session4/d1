package com.zuitt.batch193;

public class Driver {
    // property
    private String name;
    // empty constructor
    public Driver(){};
    // parameterized constructor
    public Driver(String name){
        this.name = name;
    }
    // getter
    public String getName(){
        return this.name;
    }
    // setter
    public String setName(){
        return this.name = name;
    }
}
