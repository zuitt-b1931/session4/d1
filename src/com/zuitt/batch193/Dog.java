package com.zuitt.batch193;

public class Dog extends Animal {
    // CHILD CLASS of Animal - inherits the parent
    // extends is a keyword to inherit the variables in the Animal class

    // properties
    private String breed;

    // constructor
    public Dog(){
        // super will reference the variable from the Animal class
        super(); // Animal() Constructor - by using this, we can use the instance variable, we can invoke immediate parent class constructor and class method
        this.breed = "Chihuahua";
    }

    // with parameters
    public Dog(String name, String color, String breed){
        super(name, color); // Animal(String name, String color) constructor
        this.breed = breed;
    }

    // getter
    public String getBreed(){
        return this.breed;
    }

    // method
    public void speak(){
        super.call(); // The call() method of the Animal class
        System.out.println("Bark");
    }
}
