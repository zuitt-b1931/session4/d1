package com.zuitt.batch193;

public class Car {
    // Blueprint of our Car object
    // Class Creation - composed of four parts:
        // 1. Properties - characteristics of the object
        // 2. Constructors - used to create an object
        // 3. Getters/Setters - get and set the values of each property of the object
        // 4. Methods - functions of an object where it can perform a task

    // Access Modifiers
        // 1. Default - no keyword required. only those classes that are in the same package can access this class. NO other class outside the package can access the class.
        // 2. Private - properties and methods are only accessible within the class
        // 3. Public - the members, methods, and classes that are declared public can be accessed from anywhere.
        // 4. Protected - protected data member and method are only accessible by the classes of the same package and the subclasses present in any package. You can also say that the protected access modifier is similar to default access modifier with one exception that it has visibility in subclasses.

    // Properties = qualities or characters of real world objects()
    private String name;
    private String brand;
    private int manufactureDate;
    private String owner;
    // make Driver component
    private Driver d;
    private Passenger p;

    // Constructors
    // empty constructor - it is common practice to create an empty and parameterized constructor for creating new instances
    public Car(){
        // add a driver so whenever a new car is created, it will always HAVE a driver
        this.d = new Driver("Alejandro");
        // whenever a new car is created, it will always HAVE a passenger "Lady Gaga"
        this.p = new Passenger("Lady Gaga");
    };
    // parameterized constructor = accepts properties parameters
    public Car(String name, String brand, int manufactureDate, String owner){
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

    // Getters and Setters
    // This is used for retrieving and changing the properties(write-only, read-only)

    // add a getter for driver
    public String getDriverName(){
        return this.d.getName();
    }
    // add a getter for passenger
    public String getPassengerName(){
        return this.p.getPassengerName();
    }

    // getters - read-only or just getting the properties
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getManufactureDate(){
        return this.manufactureDate;
    }
    public String getOwner(){
        return this.owner;
    }

    // setters - write-only or for setting up the properties
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setManufactureDate(int manufactureDate){
        this.manufactureDate = manufactureDate;
    }
    public void setOwner(String owner){
        this.owner = owner;
    }

    // Methods
    public void drive(){
        System.out.println(getOwner() + " drives the " + getBrand());
    }
    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by " + getOwner());
    }
}
