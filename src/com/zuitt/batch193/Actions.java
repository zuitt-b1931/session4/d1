package com.zuitt.batch193;

public interface Actions {
    // Interface is used to achieve abstraction, users of our app will be able to use the methods but not be able to see how those methods work
    // Classes cannot have multiple inheritance
    // Classes can implement multiple interface
    // can be thought of as classes for classes or blueprints for blueprints
    public void sleep();
    public void run();
}
