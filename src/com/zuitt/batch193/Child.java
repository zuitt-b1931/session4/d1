package com.zuitt.batch193;

public class Child extends Parent {
    // Override
        // the function/method is overridden by replacing the definition of the method in the parent to the child class.
    //@Override // annotation that we want to override this method
    public void speak(){
        System.out.println("I am a mere Child");
    }
}
