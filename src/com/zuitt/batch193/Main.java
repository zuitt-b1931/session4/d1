package com.zuitt.batch193;

public class Main {
    public static void main(String[] args){
        // Instantiation of object
        // new instance with an empty constructor
        Car myFirstCar = new Car();

        myFirstCar.setName("Civic");
        myFirstCar.setBrand("Honda");
        myFirstCar.setManufactureDate(1998);
        myFirstCar.setOwner("John");

        System.out.println(myFirstCar.getName());

        // new instance with parameterized parameter
        Car mySecondCar = new Car("Charger", "Dodge", 1978, "Vin Diesel");

        // we used the getters from the Car class to retrieve the property of the object
        System.out.println(mySecondCar.getName());
        System.out.println(mySecondCar.getBrand());
        System.out.println(mySecondCar.getManufactureDate());
        System.out.println(mySecondCar.getOwner());

        // Methods
        myFirstCar.drive();
        mySecondCar.drive();

        myFirstCar.printDetails();
        mySecondCar.printDetails();

        // Encapsulation
            // we bundled each field and methods inside a single class and we used access modifiers like public to allow the access from another class

            // WHY we use encapsulation
                // the fields of a class can be made read-only and write-only
                // a class can have total control over what is stored in its field
                // in Java, encapsulation helps us to keep related fields and methods together which make our code cleaner and easy to read
                // we can achieve data hiding using the access modifier private
        Car anotherCar = new Car();
        anotherCar.setName("Maranello");
        System.out.println(anotherCar.getName());

        // anotherCar.name = "Honda";
        // System.out.println(anotherCar.getName()); // RESULTS IN ERROR since 'name' is private in Car class

        // Composition = ex: A car has a driver
            // allows modelling objects that are made up of other objects. It defines "HAS a relationship".
        Car newCar = new Car();
        System.out.println("This car is driven by " + newCar.getDriverName());
        System.out.println("A car has a driver " + anotherCar.getDriverName());

        // everytime we added a new Car object, the Car() always HAS a driver and a passenger
        System.out.println("This car HAS a passenger named " + newCar.getPassengerName());

        // Inheritance = ex: A car is a vehicle
            // allows modelling objects inherit as a subset of another object. It defines "IS a relationship".
            // can be defined as the process where one class acquires the properties/(methods and fields) of another class.
            // "extends" is the keyword used to inherit the properties of a class.
            // "super" keyword is used for referencing the variable, properties or methods which is used on another class.
        Dog dog1 = new Dog();
        System.out.println(dog1.getBreed());

        dog1.setName("Brownie");
        dog1.setColor("Brown"); // setters inherited from Animal class

        System.out.println(dog1.getName());
        System.out.println(dog1.getColor()); // getters inherited from Animal class

        Dog doge = new Dog("Blackie", "Black", "Aspin");
        System.out.println(doge.getBreed());

        // method
        doge.call(); // call method inherited from Animal class
        doge.speak();

        // Interface
            // used to achieve the abstraction
            // in general terms, an interface can be defined as a container that stores the signature of methods to be implemented in the code segment
        Person jane = new Person();
        jane.run();
        jane.sleep();
        jane.morningGreet();
        jane.holidayGreet();

        // POLYMORPHISM
            // the ability of an object to take on many forms
            // there are two main types:
                // 1. Static or Compile Polymorphism
                    // usually done by function/method overloading
        StaticPoly print = new StaticPoly();
        System.out.println(print.addition(5, 5));
        System.out.println(print.addition(5, 5, 5));
        System.out.println(print.addition(5.6, 5.8));
                // 2. Dynamic Method Dispatch or Runtime Polymorphism
                    // usually done by function/method overriding.
        Parent parent1 = new Parent();
        parent1.speak(); // method of super class or parent class

        Parent subObject = new Child(); // upcasting
        subObject.speak(); // method of the subclass or child class is called by Parent reference, this is called "run time polymorphism"
    }
}
