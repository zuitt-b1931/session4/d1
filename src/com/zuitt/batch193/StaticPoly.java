package com.zuitt.batch193;

public class StaticPoly {
    public int addition(int a, int b){
    return a + b;
    }

    // OVERLOAD by adding/changing the number of arguments
    public int addition(int a, int b, int c){
        return a + b + c;
    }
    // OVERLOAD by changing the type of arguments
    public double addition(double a, double b){
        return a + b;
    }

    // method OVERLOADING: when there are multiple functions with the same name but different parameters
}
